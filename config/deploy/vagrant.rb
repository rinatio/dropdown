role :web, %w{ubuntu@vagrant}
set :settings_file, nil

before "deploy:collect_static", "deploy:ng_build"
