# config valid only for current version of Capistrano
lock '3.3.5'

set :application, 'dropdown'
set :repo_url, 'git@bitbucket.org:rinatio/dropdown.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/home/ubuntu/www/#{fetch(:application)}"

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('bin', 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  # Install virtualenv
  task :virtualenv do
    on roles(:web) do
      within release_path do
        execute "if [ ! -d #{deploy_to}/venv ]; then virtualenv -p python3 #{deploy_to}/venv; fi"
        execute "source #{deploy_to}/venv/bin/activate && pip install -r #{release_path}/requirements.txt"
      end
    end
  end

  task :restart_uwsgi do
    on roles(:web) do
      within release_path do
        execute "touch #{deploy_to}/uwsgi.ini"
      end
    end
  end

  # @link https://github.com/capistrano/capistrano/issues/719
  task :collect_static do
    on roles(:web) do
      within release_path do
        execute "source #{deploy_to}/venv/bin/activate && python #{release_path}/api/manage.py collectstatic -v0 --noinput"
      end
    end
  end

  # @link https://github.com/capistrano/capistrano/issues/719
  task :migrate do
    on roles(:web) do
      within release_path do
        execute "source #{deploy_to}/venv/bin/activate && python #{release_path}/api/manage.py migrate --noinput"
      end
    end
  end
  # NPM Build
  task :npm_build do
    on roles(:web) do
      within release_path do
        execute "cd #{release_path}/ && yarn install"
        execute "cd #{release_path}/ && npm run build-js"
        execute "cd #{release_path}/ && npm run build-css"
      end
    end
  end
end

before "deploy:updated", "deploy:virtualenv"
# after "deploy:updated", "deploy:update_settings"
# after "deploy:updated", "deploy:collect_static"
# after "deploy:published", "deploy:restart_uwsgi"
