const MinifyPlugin = require("babel-minify-webpack-plugin");

module.exports = {
    entry: {
        app: './static/src/js/app.js',
        'radix-worker': './static/src/js/radix-worker.js'
    },
    output: {
        filename: '[name].js',
        path: __dirname + '/static/dist/js',
        // filename: './static/dist/js/bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
    // plugins: [
    //     new MinifyPlugin({}, {})
    // ],
};
