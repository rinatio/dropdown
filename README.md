# Dropdown

Dropdown with following options:

- Pictures.
- Multiselect.
- Autocomplete.
- Automatic keyboard layout fix.
- Lazy-loading menu for extremely large number of menu rows.
- Additional backend search.

Find life demo [here](http://dropdown.rinat.io).

# Install

    $ mkvirtualenv --python=`which python3` dropdown
    $ pip install -r requirements.txt
    $ yarn install

# Compile

    $ npm run build-css
    $ npm run build-js

# Run

    $ cd api
    $ FLASK_APP=api.py flask db upgrade
    $ FLASK_APP=api.py flask run

# Migrations

    $ cd api
    $ mkdir db
    $ FLASK_APP=api.py flask db init
    $ FLASK_APP=api.py flask db migrate
