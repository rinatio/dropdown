onmessage = function(e) {
    let radix = {};

    e.data.forEach((i, idx) => {
        let prefix = '';
        i.text.toLowerCase().split('').forEach(letter => {
            prefix += letter;
            if (!radix.hasOwnProperty(prefix)) {
                radix[prefix] = [idx]
            } else {
                radix[prefix].push(idx);
            }
        });
    });

    postMessage({
        length: Object.keys(radix).length,
        radix: radix
    });
};
