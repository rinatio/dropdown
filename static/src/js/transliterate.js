const matches = {
    engToRu: [
        ['й', 'q'],
        ['ц', 'w'],
        ['у', 'e'],
        ['к', 'r'],
        ['е', 't'],
        ['н', 'y'],
        ['г', 'u'],
        ['ш', 'i'],
        ['щ', 'o'],
        ['з', 'p'],
        ['х', '['],
        ['х', '{'],
        ['ъ', ']'],
        ['ъ', '}'],
        ['ё', '\\'],
        ['ё', '|'],
        ['ф', 'a'],
        ['ы', 's'],
        ['в', 'd'],
        ['а', 'f'],
        ['п', 'g'],
        ['р', 'h'],
        ['о', 'j'],
        ['л', 'k'],
        ['д', 'l'],
        ['ж', ';'],
        ['ж', ':'],
        ['э', '\''],
        ['э', '"'],
        ['я', 'z'],
        ['ч', 'x'],
        ['с', 'c'],
        ['м', 'v'],
        ['и', 'b'],
        ['т', 'n'],
        ['ь', 'm'],
        ['б', ','],
        ['б', '<'],
        ['ю', '.'],
        ['ю', '>']
    ],
    translit: [
        ['a', 'а'],
        ['b', 'б'],
        ['v', 'в'],
        ['g', 'г'],
        ['d', 'д'],
        ['e', 'е'],
        ['yo', 'ё'],
        ['zh', 'ж'],
        ['z', 'з'],
        ['i', 'и'],
        ['j', 'й'],
        ['k', 'к'],
        ['l', 'л'],
        ['m', 'м'],
        ['n', 'н'],
        ['o', 'о'],
        ['p', 'п'],
        ['r', 'р'],
        ['s', 'с'],
        ['t', 'т'],
        ['u', 'у'],
        ['f', 'ф'],
        ['h', 'х'],
        ['x', 'х'],
        ['kh', 'х'],
        ['c', 'ц'],
        ['cz', 'ц'],
        ['ch', 'ч'],
        ['sh', 'ш'],
        ['shh', 'щ'],
        ['``', 'ъ'],
        ['y`', 'ы'],
        ['`', 'ь'],
        ['e`', 'э'],
        ['ju', 'ю'],
        ['yu', 'ю'],
        ['ja', 'я'],
        ['ya', 'я']
    ]
};

class Converter {
    static transliterate(word) {
        matches.translit.sort((a, b) => {
            [a, b] = [a[0], b[0]];
            return a.length < b.length ? 1 : (a.length === b.length ? 0 : -1);
        }).forEach(row => {
            word = word.replace(new RegExp(row[0], 'g'), row[1]);
        });
        return word;
    }

    static layoutEngToRu(word) {
        matches.engToRu.forEach(row => {
            const key = escapeRegExp(row[1]);
            word = word.replace(new RegExp(key, 'g'), row[0]);
        });
        return word;
    }

    static layoutRuToEng(word) {
        matches.engToRu.forEach(row => {
            const key = escapeRegExp(row[0]);
            word = word.replace(new RegExp(key, 'g'), row[1]);
        });
        return word;
    }
}

const escapePattern = /[.*+?^${}()|[\]\\]/g;
function escapeRegExp(string) {
    return string.replace(escapePattern, '\\$&'); // $& means the whole matched string
}

export {Converter}
