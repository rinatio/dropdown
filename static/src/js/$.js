/**
 * Helper class for DOM methods
 */
'use strict';

class $ {
    /**
     * Helper method to find closest parent by selector
     *
     * @param elm
     * @param selector
     * @returns {*}
     */
    static closest(elm, selector) {
        while (elm) {
            if (elm.matches(selector)) {
                return elm;
            }
            elm = elm.parentElement;
        }
        return null;
    }

    /**
     * Find next sibling matching selector
     *
     * @param elm
     * @param selector
     * @return {*}
     */
    static nextSibling(elm, selector = null) {
        let nSibling = elm.nextElementSibling;
        if (!selector) {
            return nSibling;
        } else {
            while(nSibling !== null) {
                if (nSibling.matches(selector)) {
                    return nSibling;
                }
                nSibling = nSibling.nextElementSibling;
            }
        }
        return null;
    }

    /**
     * Find previous sibling matching selector
     *
     * @param elm
     * @param selector
     * @return {*}
     */
    static prevSibling(elm, selector = null) {
        let nSibling = elm.previousElementSibling;
        if (!selector) {
            return nSibling;
        } else {
            while(nSibling !== null) {
                if (nSibling.matches(selector)) {
                    return nSibling;
                }
                nSibling = nSibling.previousElementSibling;
            }
        }
        return null;
    }

    /**
     * querySelector wrapper
     *
     * @param elm
     * @param selector
     * @return {(ElementTagNameMap[keyof ElementTagNameMap] | null) | (Element | null)}
     */
    static find(elm, selector) {
        return elm.querySelector(selector);
    }

    /**
     * querySelectorAll wrapper
     *
     * @param elm
     * @param selector
     * @return {*}
     */
    static findAll(elm, selector) {
        return elm.querySelectorAll(selector);
    }

    /**
     * Add event listener wrapper
     *
     * @param event
     * @param elm
     * @param callback
     */
    static on(event, elm, callback) {
        elm.addEventListener(event, callback);
    }

    /**
     * Remove event listener wrapper
     *
     * @param event
     * @param elm
     * @param callback
     */
    static off(event, elm, callback) {
        elm.removeEventListener(event, callback);
    }

    /**
     * XMLHttpRequest GET wrapper
     *
     * @param {string} url
     * @param {*} data
     * @param {function} clb
     */
    static post(url, data, clb) {
        let request = new XMLHttpRequest();
        request.open('POST', url, true);
        request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        request.onload = function () {
            if (request.status >= 200 && request.status < 400) {
                clb(JSON.parse(request.responseText));
            } else {
                alert(`Server error: ${request.responseText}`)
            }
        };
        request.onerror = function () {
            alert('Cannot make request.');
        };
        request.send(data);
    }

    /**
     * XMLHttpRequest GET wrapper
     *
     * @param {string} url
     * @param {function} clb
     */
    static get(url, clb) {
        let request = new XMLHttpRequest();
        request.open('GET', url, true);

        request.onload = () => {
            if (request.status >= 200 && request.status < 400) {
                let data = JSON.parse(request.responseText);
                clb(data);
            } else {
                alert('Error on users load');
            }
        };
        request.onerror = () => {
            console.log('Could not load users');
        };
        request.send();
    }

    /**
     * Window.requestAnimationFrame wrapper
     *
     * @return {number}
     */
    static raf() {
        return window.requestAnimationFrame.apply(window, arguments);
    }
}

export {$};
