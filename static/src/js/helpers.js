/**
 * Helper function for function call debounce
 *
 * @param func
 * @param delay
 * @returns {Function}
 */
function debounce(func, delay) {
    let inDebounce;
    return function () {
        const context = this;
        const args = arguments;
        clearTimeout(inDebounce);
        inDebounce = setTimeout(() => func.apply(context, args), delay)
    }
}

export {debounce};
