import {$} from './$';

/**
 * List of task functions
 *
 * @type {function[]}
 */
let taskList = [];

/**
 * Helper class to run tasks with batching, where you segment the
 * larger task into micro-tasks
 */
class TaskManager {
    /**
     * Add new tasks to the queue
     *
     * @param {function[]} tasks
     * @param {boolean} reset Whether to add new tasks or reset
     */
    static addTasks(tasks, reset=false) {
        taskList = reset ? tasks : tasks.concat(taskList);
        if (taskList.length > 0) {
            TaskManager._runTasks();
        }
    }

    /**
     * Run tasks
     *
     * @private
     */
    static _runTasks() {
        $.raf(processTaskList);

        function processTaskList(taskStartTime) {
            let taskFinishTime;

            do {
                let nextTask = taskList.pop();
                nextTask && nextTask();
                taskFinishTime = window.performance.now();
            } while (taskFinishTime - taskStartTime < 3);

            if (taskList.length > 0) {
                $.raf(processTaskList);
            }
        }
    }
}

export {TaskManager}
