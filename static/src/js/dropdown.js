/**
 * DropDown Class.
 *
 * @constructor
 * @param {Object} options - The options object.
 */

import {Converter} from "./transliterate";
import {$} from "./$";
import {ImgLoader} from "./img-loader";
import {debounce} from "./helpers";
import {TaskManager} from "./task-manager";

/**
 * CSS styles. Might need a better place.
 *
 * @type {Object}
 */
let styles = {
    wrapper: 'dd',
    label: 'dd__label',
    labelCancel: 'dd__label__cancel',
    labelAdd: 'dd__label--add',
    labelAddIcon: 'dd__label__plus',
    menuWrapper: 'dd-menu-wrapper',
    menu: 'dd-menu',
    menuHidden: 'dd-menu--hidden',
    menuNotFound: 'dd-menu--not-found',
    openIcon: 'dd__open-icon',
    input: 'dd__input',
    item: 'dd-menu__item',
    itemLazy: 'dd-menu__item--lazy',
    itemHidden: 'dd-menu__item--hidden',
    itemHighlighted: 'dd-menu__item--highlighted',
    itemImg: 'dd-menu__img',
    itemText: 'dd-menu__text',
    itemDomain: 'dd-menu__domain',
    itemNotFound: 'dd-menu__item--not-found',
    selection: 'dd__selection',
    selectionCancel: 'dd__selection__cancel',
};

styles['itemVisible'] = `.${styles.item}:not(.${styles.itemHidden}):not(.${styles.itemNotFound})`;

const keys = {
    enter: 13,
    escape: 27,
    upArrow: 38,
    downArrow: 40,
};

class DropDown {
    /**
     * Create dropdown
     *
     * @param {Object} elm
     * @param {Object} settings
     */
    constructor(elm, settings) {
        /**
         * List of all items indexes matching autocomplete search.
         * It is needed to check if we have to show menu item after multiselect label cancel
         * with input not being empty.
         *
         * @type {number[]}
         */
        this.searchResults = [];

        /**
         * List of selected items
         *
         * @type {Object[]}
         */
        this.selectedItems = [];

        /**
         * Whether menu dropdown is open or not
         *
         * @type {boolean}
         */
        this.isMenuOpen = false;

        /**
         * Map between item ID and its index in items array
         *
         * @type {{number: number}}
         */
        this.itemIdMap = {};

        /**
         * One menu item height in pixels
         *
         * @type {number}
         */
        this.itemHeight = 0;

        /**
         * Menu object
         *
         * @type {SimpleMenu|LazyMenu}
         */
        this.menu = null;

        /**
         * Flag indicating whether item was found by autocomplete or not
         *
         * @type {boolean}
         */
        this.notFound = false;

        /**
         * List of items for dropdown menu
         *
         * @type {Object[]}
         */
        this.items = [];

        /**
         * Currently highlighted element
         *
         * @type {HTMLElement}
         */
        this.highlighted = null;

        /**
         * Dropdown settings
         *
         * @type {Object}
         */
        this.settings = Object.assign({
            multiSelect: false,
            autocomplete: false,
            pictures: false,
            apiSearch: false,
            showDomain: false,
            lazyLoading: false
        }, settings);

        this._fetchApiSearch = debounce(this._fetchApiSearch, 200);
        this.items = settings.list;
        this.elm = document.createElement('div');
        elm.parentNode.insertBefore(this.elm, elm.nextSibling);
        elm.style.display = 'none';
        this._init();
    }

    /**
     * Render template and initialize elements
     * @private
     */
    _init() {
        $.raf(() => {
            if (this.settings.autocomplete) {
                initRadix.call(this);
            }
            this.elm.innerHTML = renderMainTpl.call(this);
            this.wrapper = $.find(this.elm, '.' + styles.wrapper);
            this.menuWrapper = $.find(this.elm, '.' + styles.menuWrapper);
            this.menuElm = $.find(this.elm, '.' + styles.menu);
            this._initMenuItems(this.items);
            this.openIcon = $.find(this.elm, '.' + styles.openIcon);
            this.input = $.find(this.elm, 'input:not([readonly])');
            this.placeholder = $.find(this.elm, 'input[readonly]');
            this._initListeners();
        });

        /**
         * Init radix tree for autocomplete search via web workers
         */
        function initRadix() {
            let worker = new Worker('dist/js/radix-worker.js?1');
            worker.postMessage(this.items);
            worker.onmessage = (e) => {
                this.radix = e.data.radix;
                worker.terminate();
            };
        }

        /**
         * Render main template
         *
         * @returns {string}
         */
        function renderMainTpl() {
            let input = '';
            if (this.settings.autocomplete) {
                input = `<input type="text" class="${styles.input}" placeholder="Type a name for search">`;
            } else {
                input = `<input type="text" readonly class="${styles.input}" placeholder="Select name from dropdown menu">`;
            }
            return `
                <div class="${styles.wrapper}">
                    <span class="${styles.openIcon}"></span>
                    ${input}
                </div>
                <div class="${styles.menuWrapper}">
                    <div class="${styles.menu} ${styles.menuHidden}"></div>
                    <div class="${styles.menu} ${styles.menuHidden} ${styles.menuNotFound}">
                        <div class="${styles.item} ${styles.itemNotFound}">No records</div>
                    </div>
                </div>
            `;
        }
    }

    /**
     * Init div element for each menu item, create menu required menu for items list.
     *
     * @param {Object[]} items
     * @private
     */
    _initMenuItems(items) {
        let that = this;
        items.forEach((item, idx) => {
            this.itemIdMap[parseInt(item.id, 10)] = idx;
            Object.defineProperty(item, 'elm', { // Create element dynamically by request
                get() {
                    return this._elm || (this._elm = renderMenuItem.call(that, item));
                }
            });
        });

        if (this.settings.lazyLoading) {
            this.menu = new LazyMenu(this.menuElm, items);
        } else {
            this.menu = new SimpleMenu(this.menuElm, items, {
                pictures: this.settings.pictures
            });
        }

        /**
         * Render template for menu dropdown item row
         *
         * @param {Object} item
         * @returns {Element}
         */
        function renderMenuItem(item) {
            let itemDiv = document.createElement('div');
            itemDiv.classList.add(styles.item);
            itemDiv.setAttribute('data-id', item.id);
            if (this.settings.pictures) {
                let img = document.createElement('img');
                // img.setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=');
                img.setAttribute('data-src', item.photo);
                img.classList.add(styles.itemImg);
                itemDiv.appendChild(img);
                item.img = img;
            }
            let textDiv = document.createElement('div');
            textDiv.classList.add(styles.itemText);
            textDiv.appendChild(document.createTextNode(item.text));
            if (this.settings.showDomain) {
                let div = document.createElement('div');
                let small = document.createElement('small');
                small.classList.add(styles.itemDomain);
                small.appendChild(document.createTextNode(item.domain));
                div.appendChild(small);
                textDiv.appendChild(div);
            }
            itemDiv.appendChild(textDiv);
            return itemDiv;
        }
    }

    /**
     * Attach required event listeners
     *
     * @private
     */
    _initListeners() {
        this._onDocumentClick = this._onDocumentClick.bind(this);
        this._onOpenIconClick = this._onOpenIconClick.bind(this);
        this._onInput = this._onInput.bind(this);
        this._onMenuItemClick = this._onMenuItemClick.bind(this);
        this._onLabelCancelClick = this._onLabelCancelClick.bind(this);
        this._onSelectionCancelClick = this._onSelectionCancelClick.bind(this);
        this._onKeyDown = this._onKeyDown.bind(this);
        this._onMouseOver = this._onMouseOver.bind(this);
        this._onMouseDown = this._onMouseDown.bind(this);

        $.on('click', document, this._onDocumentClick);
        $.on('click', this.elm, this._onOpenIconClick);
        if (this.settings.autocomplete) {
            $.on('mousedown', this.elm, this._onMouseDown);
            $.on('input', this.input, this._onInput);
            $.on('keydown', this.elm, this._onKeyDown);
        }
        $.on('click', this.elm, this._onMenuItemClick);
        $.on('click', this.elm, this._onLabelCancelClick);
        $.on('click', this.elm, this._onSelectionCancelClick);
        $.on('mouseover', this.elm, this._onMouseOver);
    }

    /**
     * Remove event listeners
     * @private
     */
    _removeListeners() {
        $.off('click', document, this._onDocumentClick);
        $.off('click', this.elm, this._onOpenIconClick);
        if (this.settings.autocomplete) {
            $.off('mousedown', this.elm, this._onMouseDown);
            $.off('input', this.input, this._onInput);
            $.off('keydown', this.elm, this._onKeyDown);
        }
        $.off('click', this.elm, this._onMenuItemClick);
        $.off('click', this.elm, this._onLabelCancelClick);
        $.off('click', this.elm, this._onSelectionCancelClick);
        $.off('mouseover', this.elm, this._onMouseOver);
    }

    /**
     * Document click handler
     * Show/hide menu list
     *
     * @param e
     * @private
     */
    _onDocumentClick(e) {
        if (this.menuWrapper.contains(e.target)) {
            return;
        }
        if (this.elm.contains(e.target)) {
            if (!this.openIcon.contains(e.target)) {
                this._toggleMenu(true);
            }
        } else if (document.contains(e.target)) {
            this._toggleMenu(false);
        }
    }

    /**
     * Dropdown open icon click handler
     *
     * @param e
     * @private
     */
    _onOpenIconClick(e) {
        if (e.target.contains(this.openIcon)) {
            this._toggleMenu(!this.isMenuOpen);
        }
    }

    /**
     * Dropdown mousedown event handler.
     *
     * Prevent default if we click on menu item or cancel label if input is focused
     * so it does not loose focus.
     *
     * @param e
     * @private
     */
    _onMouseDown(e) {
        if (e.target.classList.contains(styles.labelCancel) || this.menuElm.contains(e.target)) {
            if (document.activeElement === this.input) {
                e.preventDefault();
            }
        }
    }

    /**
     * Input event handler for autocomplete.
     * Find matching records and update menu list
     *
     * @param e
     * @private
     */
    _onInput(e) {
        let val = e.target.value;
        let that = this;

        $.raf(() => {
            this.searchResults = [];

            setTimeout(() => this.menuElm.scrollTop = 0, 0);

            if (0 === val.length) {
                let notSelectedItems = this.items.filter(i => !i.selected);
                this.menu.resetItems(notSelectedItems);
                this._toggleNotFound(notSelectedItems.length === 0);
                return;
            }

            let variants = getSearchVariants(val);
            let rdxSearchIndices = getSearchMatchingIndices(variants);
            let rdxMatchingItems = rdxSearchIndices.map(idx => this.items[idx]).filter(i => !i.selected);

            if (0 === rdxMatchingItems.length) {
                !this.settings.apiSearch && this._toggleNotFound(true);
            } else {
                this.menu.resetItems(rdxMatchingItems);
                this._toggleNotFound(false);
                this.searchResults = this.searchResults.concat(rdxSearchIndices);
            }

            if (!this.settings.apiSearch) {
                return;
            }

            this._fetchApiSearch(variants, data => {
                if (val !== e.target.value) {
                    return;
                }
                let reset = 0 === rdxSearchIndices.length;

                let apiSearchIndices = data.reduce((accumulator, itemId) => {
                    let idx = this._findItemIdxById(itemId);
                    if (null !== idx && -1 === rdxSearchIndices.indexOf(idx)) {
                        accumulator.push(idx);
                    }
                    return accumulator;
                }, []);

                if (apiSearchIndices.length > 0) {
                    let apiResults = apiSearchIndices.sort().map(idx => this.items[idx]).filter(i => !i.selected);
                    if (reset) {
                        this.menu.resetItems(apiResults);
                    } else {
                        this.menu.insertItems(apiResults);
                    }
                    this._toggleNotFound(apiResults.length === 0 && rdxMatchingItems.length === 0);
                    this.searchResults = this.searchResults.concat(apiSearchIndices);
                } else if (reset) {
                    this._toggleNotFound(true);
                }
            });
        });

        /**
         * Get multiple variants for search by one string with wrong layout, transliteration etc
         *
         * @param {string} str
         * @returns {string[]}
         */
        function getSearchVariants(str) {
            let s1 = str.toLowerCase();
            let s2 = Converter.transliterate(s1);
            let s3 = Converter.layoutEngToRu(s1);
            let s4 = Converter.layoutRuToEng(s1);
            let s5 = Converter.transliterate(s4);
            let s6 = s1.replace('е', 'ё');
            let s7 = s3.replace('е', 'ё');
            return [s1, s2, s3, s4, s5, s6, s7].filter((item, pos, self) => {
                // Remove duplicates
                return self.indexOf(item) === pos;
            });
        }

        /**
         * Get matching items indices array for search variants via radix tree
         *
         * @param {string[]} variants List of search string variants
         * @return {number[]} Matching indices
         */
        function getSearchMatchingIndices(variants) {
            let indices = [];
            variants.forEach(search => {
                if (that.radix.hasOwnProperty(search)) {
                    indices = indices.concat(that.radix[search]);
                }
            });
            return indices;
        }

    }

    /**
     * Fetch search results from back end API
     *
     * @param variants List of search string variants
     * @param clb Success callback
     */
    _fetchApiSearch(variants, clb) {
        $.post('/api/friends/search', JSON.stringify({
            search: variants
        }), clb);
    }

    /**
     * Show or hide "not found" placeholder
     *
     * @param {boolean} toggle
     * @private
     */
    _toggleNotFound(toggle) {
        if (toggle !== this.notFound) {
            this.notFound = toggle;
            this.menuElm.nextElementSibling.classList.toggle(styles.menuHidden, !toggle);
            this.menuElm.classList.toggle(styles.menuHidden, toggle);
        }
    }

    /**
     * Show/hide menu list
     *
     * @param {boolean} toggle
     * @private
     */
    _toggleMenu(toggle) {
        if (this.isMenuOpen === toggle) {
            return;
        }
        if (this.notFound) {
            this.menuElm.nextElementSibling.classList.toggle(styles.menuHidden, !toggle);
        } else {
            this.menuElm.classList.toggle(styles.menuHidden, !toggle);
        }
        if (toggle) {
            this._togglePlusLabel(false);
        } else if (this.selectedItems.length > 0 && this.settings.multiSelect) {
            this._togglePlusLabel(this.selectedItems.length < this.items.length);
        }
        if (this.settings.autocomplete) {
            if (toggle && (this.settings.multiSelect || 0 === this.selectedItems.length)) {
                this._toggleInput(true);
                this.input.focus();
            }
            if (false === toggle && (this.selectedItems.length > 0)) {
                this._toggleInput(false);
            }
        }
        this.isMenuOpen = toggle;
    }

    /**
     * Show/hide input in dropdown
     *
     * @param {boolean} toggle
     * @private
     */
    _toggleInput(toggle) {
        this.input.style.display = toggle ? 'block' : 'none';
    }

    /**
     * - Drop-down menu item click handler.
     * - Keyboard Enter keydown handler with synthetic event.
     *
     * Check if item ID presented and call appropriate select[Multi|One]Item method
     *
     * @param {Object} e
     * @private
     */
    _onMenuItemClick(e) {
        $.raf(() => {
            let itemElm = $.closest(e.target, '.' + styles.item), id;
            if (!itemElm || !(id = itemElm.getAttribute('data-id'))) {
                return;
            }
            if (this.highlighted === itemElm) {
                itemElm.classList.remove(styles.itemHighlighted);
                this.highlighted = null;
            }
            let idx = this._findItemIdxById(parseInt(id, 10));
            let item = this.items[idx];
            if (null === idx) {
                return;
            }
            if (this.settings.multiSelect) {
                if (item.selected) {
                    return;
                }
                selectMultiItem.call(this, item);
                item.selected = true;
                this.menu.removeItem(item);
                if (!this.menu.getLength() > 0) {
                    this._toggleNotFound(true);
                }
            } else {
                selectOneItem.call(this, item);
                if (this.settings.autocomplete && this.input.value.length > 0) {
                    resetInput(this.input);
                }
                this._toggleMenu(false);
            }
            if (!this.settings.autocomplete) {
                this._togglePlaceholder(false);
            }
        });

        /**
         * Reset input to empty value and manually trigger 'input' event
         *
         * @param input
         */
        function resetInput(input) {
            input.value = '';
            let event = document.createEvent('Event');
            event.initEvent('input', true, true);
            input.dispatchEvent(event);
        }

        /**
         * Select one item for multi-select dropdown and render its label
         *
         * @param {Object} item
         */
        function selectMultiItem(item) {
            this.selectedItems.push(item);
            this.wrapper.insertBefore(renderItemLabel(item), this._plusLabel || this.input);
        }

        /**
         * Render label element for multi-select dropdown
         *
         * @param item
         * @returns {Element}
         */
        function renderItemLabel(item) {
            let div = document.createElement('div');
            div.classList.add(styles.label);
            div.setAttribute('data-id', item.id);
            div.innerHTML = `${item.text}<i class="${styles.labelCancel}">&times;</i>`;
            return div;
        }

        /**
         * Select single-item in dropdown
         *
         * @param {Object} item
         */
        function selectOneItem(item) {
            this.selectedItems = [item];
            if (this.selection) {
                this.selection.remove();
            }
            this.selection = renderSelection(item);
            this.wrapper.insertBefore(this.selection, this.input);
        }

        /**
         * Render element for singe-select selection item
         *
         * @param item
         * @returns {Element}
         */
        function renderSelection(item) {
            let div = document.createElement('div');
            div.classList.add(styles.selection);
            div.innerHTML = `${item.text}<i class="${styles.selectionCancel}">&times;</i>`;
            return div;
        }
    }

    /**
     * Helper for finding item index in items list by its ID
     *
     * @param {number} id
     * @returns {number|null}
     * @private
     */
    _findItemIdxById(id) {
        return this.itemIdMap.hasOwnProperty(id) ? this.itemIdMap[id] : null;
    }

    /**
     * Click handler for X icon on multi-select label
     *
     * @param e
     * @private
     */
    _onLabelCancelClick(e) {
        let that = this;

        if (e.target.classList.contains(styles.labelCancel)) {
            let label = e.target.parentNode;
            let id = parseInt(label.getAttribute('data-id'), 10);
            let idx = findSelectedById(id);
            if (null === idx) {
                return;
            }
            let item = this.selectedItems.splice(idx, 1).pop();
            item.selected = false;
            if (!this.settings.autocomplete || ifItemMatchSearch(item)) {
                this.menu.insertItem(item);
                this._toggleNotFound(false);
            }
            this._togglePlusLabel(this.selectedItems.length > 0 && !this.isMenuOpen);
            if (0 === this.selectedItems.length) {
                if (this.settings.autocomplete) {
                    this._toggleInput(true);
                } else {
                    this._togglePlaceholder(true);
                }
            }
            label.remove();
        }

        /**
         * Find item index in selected items by its ID
         *
         * @param {number} id
         * @return {?number}
         */
        function findSelectedById(id) {
            for (let i = 0, len = that.selectedItems.length; i < len; i++) {
                if (that.selectedItems[i].id === id) {
                    return i;
                }
            }
            return null;
        }

        /**
         * Figure out if item matches current autocomplete input or not
         *
         * @param {Object} item
         * @return {boolean}
         */
        function ifItemMatchSearch(item) {
            if ('' === that.input.value) {
                return true;
            }
            let idx = that._findItemIdxById(item.id);
            return null !== idx && -1 !== that.searchResults.indexOf(idx);
        }
    }

    /**
     * Handler for X icon click on single-select dropdown
     *
     * @param e
     * @private
     */
    _onSelectionCancelClick(e) {
        if (e.target.classList.contains(styles.selectionCancel)) {
            let selection = e.target.parentNode;
            this.selectedItems = [];
            if (this.settings.autocomplete) {
                this._toggleInput(true);
            } else {
                this._togglePlaceholder(true);
            }
            selection.remove();
        }
    }

    /**
     * Toggle placeholder text on dropdowns without autocomplete
     *
     * @param {boolean} toggle
     * @private
     */
    _togglePlaceholder(toggle) {
        this.placeholder.style.display = toggle ? 'block' : 'none';
    }

    /**
     * Show/hide plus label for multi-select drodpown
     *
     * @param {boolean} toggle
     * @private
     */
    _togglePlusLabel(toggle) {
        if (true === toggle && this._plusLabel) {
            this._plusLabel.style.display = 'inline-block';
            return;
        }
        if (false === toggle) {
            if (this._plusLabel) {
                this._plusLabel.style.display = 'none';
            }
            return;
        }
        this._plusLabel = renderPlusLabel();
        this.wrapper.insertBefore(this._plusLabel, this.input);

        /**
         * Render plus label for multi-select dropdown
         *
         * @returns {Element}
         * @private
         */
        function renderPlusLabel() {
            let div = document.createElement('div');
            div.classList.add(styles.label, styles.labelAdd);
            div.innerHTML = `Add <span class="${styles.labelAddIcon}">+</span>`;
            return div;
        }
    }

    /**
     * Input keydown event handler
     *
     * @param e
     * @private
     */
    _onKeyDown(e) {
        const pressedKey = e.which;
        if (!Object.keys(keys).some(k => keys[k] === pressedKey)) { // Check if e.which in keys map
            return;
        }

        let hElm = this.highlighted;
        if (hElm && (!this.menuElm.contains(hElm) || hElm.matches('.' + styles.itemHidden))) {
            hElm.classList.remove(styles.itemHighlighted);
            hElm = null;
            this.highlighted = null;
        }

        if (pressedKey === keys.enter && hElm) {
            this._onMenuItemClick({target: hElm});
            e.preventDefault();
        }

        if (pressedKey === keys.upArrow || pressedKey === keys.downArrow) {
            let newToHighlight = null;

            if (hElm) {
                let method = pressedKey === keys.upArrow ? 'prevSibling' : 'nextSibling';
                newToHighlight = $[method](hElm, styles.itemVisible);
            } else {
                newToHighlight = $.find(this.menuElm, styles.itemVisible);
            }

            if (newToHighlight !== null) {
                if (hElm) {
                    hElm.classList.remove(styles.itemHighlighted);
                }
                newToHighlight.classList.add(styles.itemHighlighted);
                scrollToItem.call(this, newToHighlight);
                this.highlighted = newToHighlight;
            }
            e.preventDefault();
        }

        if (pressedKey === keys.escape) {
            this._toggleMenu(false);
        }

        /**
         * Scroll menu to the item element
         *
         * @param {HTMLElement} item
         */
        function scrollToItem(item) {
            const menu = this.menuElm;
            const belowPage = menu.scrollTop + menu.clientHeight <= item.offsetTop;
            const abovePage = item.offsetTop < menu.scrollTop;
            if (belowPage) {
                menu.scrollTop = item.offsetTop + item.offsetHeight - menu.clientHeight;
            } else if (abovePage) {
                menu.scrollTop = item.offsetTop;
            }
        }
    }

    /**
     * Item mouseover event handler
     *
     * @param e
     * @private
     */
    _onMouseOver(e) {
        if (this.menuElm.contains(e.target)) {
            let itemElm = $.closest(e.target, styles.itemVisible);
            if (itemElm) {
                if (this.highlighted) {
                    this.highlighted.classList.remove(styles.itemHighlighted);
                }
                itemElm.classList.add(styles.itemHighlighted);
                this.highlighted = itemElm;
            }
        }
    }

    /**
     * Get dropdown value
     *
     * @returns {Object[]}
     */
    value() {
        return this.selectedItems;
    }

    /**
     * Delete dropdown element and remove event listeners
     */
    destroy() {
        this.menu.destroy();
        this._removeListeners();
        this.elm.remove();
        delete this.elm;
    }
}

/**
 * Abstract class for dropdown Menu subclasses
 */
class Menu {
    constructor(elm, items, options) {
        /**
         * Dropdown menu DOM element
         *
         * @type {Element}
         */
        this.rootElm = null;

        /**
         * List of menu items
         *
         * @type {Object[]}
         */
        this.items = [];

        this.rootElm = elm;
        this.items = items;
    }

    /**
     * Reset menu from scratch
     *
     * @param {Object[]} items
     */
    resetItems(items) {
        throw Error('Not implemented');
    }

    /**
     * Insert list of items to the existing menu items list
     * @param items
     */
    insertItems(items) {
        throw Error('Not implemented');
    }

    /**
     * Remove one item from menu
     *
     * @param {Object} itemToRemove
     */
    removeItem(itemToRemove) {
        throw Error('Not implemented');
    }

    /**
     * Add one item to the menu
     *
     * @param {Object} itemToAdd
     */
    insertItem(itemToAdd) {
        throw Error('Not implemented');
    }

    /**
     * Get length of menu items matching current state
     */
    getLength() {
        throw Error('Not implemented');
    }

    /**
     * Destroy menu.
     */
    destroy() {
        throw Error('Not implemented');
    }
}

/**
 * Simple menu that renders items all together
 */
class SimpleMenu extends Menu {
    constructor(elm, items, options) {
        super(elm, items);
        if (options.pictures) {
            this.imgLoader = initImageLoader.call(this);
        }

        $.raf(() => {
            let df = document.createDocumentFragment();
            items.forEach(item => df.appendChild(item.elm));
            this.rootElm.insertBefore(df, null);

            items.forEach(item => {
                item.show = true;
                if (this.imgLoader) {
                    this.imgLoader.addImage(item.img);
                }
            });
            this._updateImagesIfNoObservable();
        });


        /**
         * Init image lazy loader from data-src for item pictures
         *
         * @private
         */
        function initImageLoader() {
            let imgLoader = new ImgLoader(this.rootElm);
            if (imgLoader.supportsObserver()) {
                imgLoader.initObserver();
            } else {
                $.on('scroll', this.rootElm, imgLoader.updateImages);
            }
            return imgLoader;
        }
    }

    /**
     * Force image update without scroll if there's no observable support
     *
     * @private
     */
    _updateImagesIfNoObservable() {
        if (this.imgLoader && !this.imgLoader.observer) {
            this.imgLoader.updateImages();
        }
    }

    /**
     * Implement Menu interface.
     * Reset search results in menu dropdown after successful search
     *
     * @param {Object[]} items
     */
    resetItems(items) {
        let itemIdsMap = {};
        items.forEach(item => itemIdsMap[item.id] = true);
        this.items.forEach(i => this._toggleMenuItem(i, itemIdsMap[i.id]));
        this._updateImagesIfNoObservable();
    }

    /**
     * Implement Menu interface.
     * Show provided list of items in menu
     *
     * @param {Object[]} items
     * @return {Object}
     */
    insertItems(items) {
        items.forEach(i => this._toggleMenuItem(i, true));
        this._updateImagesIfNoObservable();
    }

    /**
     * Implement Menu interface.
     *
     * @param {Object} itemToRemove
     */
    removeItem(itemToRemove) {
        this._toggleMenuItem(itemToRemove, false);
    }

    /**
     * Implement Menu interface.
     *
     * @param {Object} itemToAdd
     */
    insertItem(itemToAdd) {
        this._toggleMenuItem(itemToAdd, true);
    }

    /**
     * Implement Menu interface.
     */
    getLength() {
        return this.items.filter(item => item.show).length;
    }

    /**
     * Toggle item "hidden" state in dropdown menu
     *
     * @param {Object} item
     * @param {boolean} toggle
     * @private
     */
    _toggleMenuItem(item, toggle) {
        if (toggle !== item.show) {
            item.show = toggle;
            item.elm.classList.toggle(styles.itemHidden, !item.show);
        }
    }

    /**
     * Implement Menu interface.
     */
    destroy() {
        if (this.imgLoader && !this.imgLoader.observer) {
            $.off('scroll', this.rootElm, this.imgLoader.updateImages);
        }
    }
}

/**
 * Helper class to manage menu items for Dropdown with lazy loading
 */
class LazyMenu extends Menu {
    constructor(elm, items, options) {
        items.forEach((i, idx) => i.idx = idx); // item.idx needed to maintain order
        super(elm, items.slice());
        /**
         * Map of rendered item indices
         *
         * @type {{}}
         */
        this.rendered = {};

        /**
         * One menu item height in pixels
         * @type {number}
         */
        this.itemHeight = 0;

        /**
         * Start and end indices of current menu dropdown frame based on scroll.
         *
         * @type {{idxBottom: number, idxTop: number, idxStart: number, idxEnd: number}}
         */
        this.frame = null;

        /**
         * Last {@link rootElm} scrollTop position
         *
         * @type {number}
         */
        this.lastScrollTop = 0;

        this.items.forEach(item => item.elm.classList.add(styles.itemLazy));
        this.rootElm.appendChild(createHeightElm());
        this.itemHeight = calculateItemHeight.call(this);

        this._resetHeight(items.length);
        this.updateVisibleItems('DOWN');

        this._onScrollLazyLoad = this._onScrollLazyLoad.bind(this);
        $.on('scroll', this.rootElm, this._onScrollLazyLoad);

        /**
         * Create virtual div element in menu dropdown that handles its height
         *
         * @return {HTMLDivElement}
         */
        function createHeightElm() {
            let heightElm = document.createElement('div');
            heightElm.setAttribute('style', 'width: 1px; float:right');
            return heightElm;
        }

        /**
         * Create one menu item and get its height in pixels
         *
         * @return {number}
         */
        function calculateItemHeight() {
            let firstItem = items[0];
            elm.appendChild(firstItem.elm);
            this._updateItemPosition(firstItem, 0);
            return firstItem.elm.offsetHeight;
        }
    }

    /**
     * Lazy load menu items on menu scroll
     */
    _onScrollLazyLoad() {
        let scrollTop = this.rootElm.scrollTop;
        let scrollDirection = scrollTop < this.lastScrollTop ? 'UP' : 'DOWN';
        this.lastScrollTop = this.rootElm.scrollTop;
        $.raf(() => {
            if (scrollTop === this.rootElm.scrollTop) {
                this.updateVisibleItems(scrollDirection);
            }
        });
    }

    /**
     * Calculate dropdown menu scroll position and render only menu items
     * required for this frame plus some extra items on top and bottom for smooth scrolling
     *
     * @param {'UP'|'DOWN'} scrollDirection
     */
    updateVisibleItems(scrollDirection = 'DOWN') {
        let scrollTop = this.rootElm.scrollTop;
        this.frame = getIdxFrame.call(this);
        renderFrameItems.call(this);
        this._removeItemsOutsideFrame(scrollDirection);

        /**
         * Get frame of items required to show for current scroll position plus N extra items
         * on the top and on the bottom.
         *
         * @return {{idxBottom: number, idxTop: number, idxStart: number, idxEnd: number}}
         */
        function getIdxFrame() {
            const dataLen = this.items.length;
            const itemHeight = this.itemHeight;
            const extraItems = 20;

            let idxTop = scrollTop / itemHeight;
            let idxBottom = (scrollTop + this.rootElm.clientHeight) / itemHeight;

            idxTop = Math.floor(Math.max(idxTop, 0));
            idxBottom = Math.floor(Math.min(idxBottom, dataLen - 1));

            let idxStart = Math.max(idxTop - extraItems, 0);
            let idxEnd = Math.min(idxBottom + extraItems, dataLen - 1);

            return {idxTop, idxBottom, idxStart, idxEnd};
        }

        /**
         * Render items inside current frame plus some extra items
         */
        function renderFrameItems() {
            let df = document.createDocumentFragment();
            let frame = this.frame;
            for (let i = frame.idxStart; i <= frame.idxEnd; i++) {
                let item = this.items[i];
                if (item.img && i <= frame.idxBottom && i >= frame.idxTop) {
                    item.img.src = item.img.getAttribute('data-src');
                    delete item.img;
                }
                if (!item.rendered) {
                    df.appendChild(item.elm);
                    this._updateItemPosition(item, i);
                }
            }
            this.rootElm.appendChild(df);
        }
    }

    /**
     * Implement Menu interface.
     *
     * @param {Object[]} items
     */
    resetItems(items) {
        Object.keys(this.rendered).forEach(idx => this._removeItemByIdx(+idx));
        this.items = items.slice(0);
        // Note: order is important for item return after label cancel
        this.items.sort((a, b) => a.idx > b.idx ? 1 : -1);
        this._resetHeight(items.length);
        this.updateVisibleItems('DOWN');
    }

    /**
     * Implement Menu interface.
     *
     * @param {Object[]} items
     */
    insertItems(items) {
        this.resetItems(this.items.concat(items));
    }

    /**
     * Update height of virtual element in menu that maintains its height by number of lazy items
     *
     * @param {number} itemsCount
     * @private
     */
    _resetHeight(itemsCount) {
        this.rootElm.firstElementChild.style.height = (this.itemHeight * itemsCount) + 'px';
    }

    /**
     * Remove items outside of current menu scroll frame
     *
     * @param {'DOWN'|'UP'} scrollDirection
     * @private
     */
    _removeItemsOutsideFrame(scrollDirection = 'DOWN') {
        let indices = Object.keys(this.rendered).filter(idx => {
            return idx < this.frame.idxStart || idx > this.frame.idxEnd;
        });
        if ('DOWN' === scrollDirection) {
            indices.reverse();
        }
        let tasks = indices.map((idx) => () => {
            this._removeItemByIdx(+idx);
        });
        TaskManager.addTasks(tasks, true);
    }

    /**
     * Remove one item from the list by its index
     *
     * @param {number} idx
     * @private
     */
    _removeItemByIdx(idx) {
        let item = this.items[idx];
        item.elm.remove();
        item.rendered = false;
        delete this.rendered[idx];
    }

    /**
     * Set menu items absolute position by its index in lazy items array
     *
     * @param {Object} item
     * @param {number} idx
     * @private
     */
    _updateItemPosition(item, idx) {
        item.rendered = true;
        item.elm.style.top = idx * this.itemHeight + 'px';
        this.rendered[idx] = true;
    }

    /**
     * Implement Menu interface.
     * Remove item from the list and update position of all other items below.
     *
     * @param {Object} itemToRemove
     */
    removeItem(itemToRemove) {
        let idx = this.items.indexOf(itemToRemove);
        this._removeItemByIdx(idx);

        Object.keys(this.rendered).filter(key => key > idx).sort((a, b) => a - b).forEach(index => {
            delete this.rendered[index];
            this._updateItemPosition(this.items[index], index - 1);
        });
        this.items.splice(idx, 1);
        this._resetHeight(this.items.length);
        this.updateVisibleItems();
    }

    /**
     * Implement Menu interface.
     * Add new item to the list preserving its original index in items list
     * Update position of all other items below.
     *
     * @param {Object} itemToAdd
     */
    insertItem(itemToAdd) {
        let idx = getInsertIndex.call(this);

        Object.keys(this.rendered).filter(key => key >= idx).sort((a, b) => b - a).forEach(index => {
            index = parseInt(index, 10);
            delete this.rendered[index];
            this._updateItemPosition(this.items[index], index + 1);
        });

        this.items.splice(idx, 0, itemToAdd);
        this._updateItemPosition(itemToAdd, idx);
        this.rootElm.insertBefore(itemToAdd.elm, getInsertBeforeElm.call(this, idx + 1));

        this._resetHeight(this.items.length);
        this.updateVisibleItems();

        /**
         * Get next element to insert item before
         *
         * @param {number} startIdx
         * @return {HTMLElement}
         */
        function getInsertBeforeElm(startIdx) {
            for (let i = startIdx, len = this.items.length; i < len; i++) {
                if (this.items[i].rendered) {
                    return this.items[i].elm;
                }
            }
            return null;
        }

        /**
         * Get index in items list to insert new item in
         *
         * @return {number}
         */
        function getInsertIndex() {
            let items = this.items;
            for (let i = 0, len = items.length; i < len; i++) {
                if (items[i].idx > itemToAdd.idx) {
                    return i;
                }
            }
            return items.length;
        }
    }

    /**
     * Implement Menu interface.
     */
    getLength() {
        return this.items.length;
    }

    /**
     * Implement Menu interface.
     */
    destroy() {
        $.off('scroll', this.rootElm, this._onScrollLazyLoad);
    }
}

export {DropDown}
