/**
 * Observe images and set src from data-src once it is visible.
 */
import {debounce} from "./helpers";


class ImgLoader {
    /**
     *
     * @param elm Element to listen scroll on if IntersectionObserver is not available
     */
    constructor(elm) {
        this.elm = elm;
        this.images = [];
        this.observer = null;
        this.updateImages = debounce(this.updateImages.bind(this), 50);
    }

    addImage(newImage) {
        if (this.observer) {
            this.observer.observe(newImage);
        } else {
            this.images.push(newImage);
        }
    }

    updateImages() {
        let that = this;
        window.requestAnimationFrame(() => {
            this.images = this.images.filter(img => {
                if (imgInViewport(img)) {
                    img.src = img.getAttribute('data-src');
                    return false;
                }
                return true;
            });
        });
        function imgInViewport(img) {
            if (!img.offsetHeight) {
                return false;
            }
            const belowPage = that.elm.scrollTop + that.elm.clientHeight <= img.offsetTop;
            const abovePage = img.offsetTop + img.offsetHeight < that.elm.scrollTop;
            return !(belowPage || abovePage);
        }
    }

    initObserver() {
        function callback(entries, observer) {
            entries.forEach(entry => {
                if (entry.intersectionRatio > 0) {
                    observer.unobserve(entry.target);
                    entry.target.src = entry.target.getAttribute('data-src');
                }
            })
        }
        // callback = debounce(callback, 50);
        this.observer = new IntersectionObserver(callback);
    }

    supportsObserver() {
        return !!window.IntersectionObserver;
    }
}

export {ImgLoader}
