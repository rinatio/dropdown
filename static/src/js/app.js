'use strict';
import {DropDown} from "./dropdown";
import {$} from "./$";

document.addEventListener("DOMContentLoaded", () => {
    $.get('users.json', data => {
        let selects = document.querySelectorAll('select');
        Array.prototype.forEach.call(selects, select => {
            $.raf(() => {
                let limit = parseInt(select.getAttribute('data-limit'), 10) || undefined;
                new DropDown(select, {
                    list: data.map(u => {
                        return Object.assign({text: u.first_name + ' ' + u.last_name}, u)
                    }).slice(0, limit),
                    autocomplete: select.getAttribute('data-autocomplete') > 0,
                    multiSelect: select.hasAttribute('multiple'),
                    pictures: select.getAttribute('data-pictures') > 0,
                    apiSearch: select.getAttribute('data-api-search') > 0,
                    showDomain: select.getAttribute('data-domain') > 0,
                    lazyLoading: select.getAttribute('data-lazy') > 0,
                });
            });
        });
    });
});
