import hashlib

import time
from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from sqlalchemy import func

from common.cache import client as memcache


app = Flask(__name__, static_url_path='/static', static_folder='../static')

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db/app.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)
migrate = Migrate(app, db)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String())
    last_name = db.Column(db.String())
    domain = db.Column(db.String())
    photo = db.Column(db.String())

    @staticmethod
    def searchable():
        return 'id', 'first_name', 'last_name', 'domain', 'photo'


@app.route("/")
def hello():
    return "hello"


@app.route("/api/friends")
def friends():
    users = User.query.all()
    response = []
    for user in users:
        response.append({
            "id": user.id,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "domain": user.domain,
            "photo": user.photo,
        })
    return jsonify(response)


@app.route("/api/friends/search", methods=['POST'])
def search_friend():
    data = request.get_json()
    if 'search' not in data or data['search'] is None:
        return jsonify([])
    search = data['search']
    variants = search if isinstance(search, list) else [search]
    if len(variants) == 0:
        return jsonify([])

    start = time.time()
    search_hash = hashlib.md5(', '.join(variants).encode('utf-8')).hexdigest()
    cached_response = memcache.get(search_hash)
    if cached_response:
        print("Get from cache. Completed", time.time() - start)
        return jsonify(cached_response)

    user_filter = None
    for variant in variants:
        if user_filter is None:
            user_filter = func.lower(User.domain).startswith(str(variant), autoescape=True)
        else:
            user_filter |= func.lower(User.domain).startswith(str(variant), autoescape=True)

    users = User.query.with_entities(User.id).filter(user_filter).all()
    response = []
    for user in users:
        response.append(user.id)
    print("Get from DB. Completed", time.time() - start)
    memcache.set(search_hash, response)
    return jsonify(response)


if __name__ == "__main__":
    app.run(host='0.0.0.0')
